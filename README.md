Bullet hell game developed as a proof-of-concept for Pascal game programming, using SDL2

Controls:

* arrow keys: move ship
* Z: shoot

Tested in Windows and Linux.
