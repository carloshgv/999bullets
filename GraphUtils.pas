unit GraphUtils;
interface
uses SDL2, SDL2_ttf, math;

type
	UT_Poly = record
		x, y, sides : integer;
		radius, angle : real;
	end;
	PUT_Poly = ^UT_Poly;

procedure UT_DrawPoly(sdlRenderer : PSDL_Renderer; poly : PUT_Poly);
function UT_Clamp(var x : integer; min, max : integer) : boolean;
function UT_Clampf(var x : single; min, max : single) : boolean;
procedure UT_Write(sdlRenderer : PSDL_Renderer; font : PTTF_Font; text : string; rect : PSDL_Rect; color : PSDL_Color);
function UT_TicksToStr(t : cardinal) : string;

implementation
uses sysutils;

procedure UT_DrawPoly(sdlRenderer : PSDL_Renderer; poly : PUT_Poly);
var
	i : integer;
	px, py, ppx, ppy : real;
	arc : real;
begin
	i := 0;
	arc := 2*Pi / poly^.sides;
	ppx := sin(arc * i + poly^.angle) * poly^.radius + poly^.x;
	ppy := cos(arc * i + poly^.angle) * poly^.radius + poly^.y;
	for i := 1 to poly^.sides do
	begin
		px := sin(arc * i + poly^.angle) * poly^.radius + poly^.x;
		py := cos(arc * i + poly^.angle) * poly^.radius + poly^.y;
		SDL_RenderDrawLine(sdlRenderer, trunc(ppx), trunc(ppy), trunc(px), trunc(py));
		ppx := px;
		ppy := py;
	end;
end;

function UT_Clamp(var x : integer; min, max : integer) : boolean;
begin
	if x < min then
	begin
		x := min;
		UT_Clamp := true;
	end
	else if x > max then
	begin
		x := max;
		UT_Clamp := true;
	end
	else
		UT_Clamp := false;
end;

function UT_Clampf(var x : single; min, max : single) : boolean;
begin
	if x < min then
	begin
		x := min;
		UT_Clampf := true;
	end
	else if x > max then
	begin
		x := max;
		UT_Clampf := true;
	end
	else
		UT_Clampf := false;
end;

procedure UT_Write(sdlRenderer : PSDL_Renderer; font : PTTF_Font; text : string; rect : PSDL_Rect; color : PSDL_Color);
var
	surface : PSDL_Surface;
	texture : PSDL_Texture;
	x, y : integer;
	
begin
	surface := TTF_RenderText_Solid(font, pchar(@text)+1, color^);
	if surface = nil then
		writeln('failed write: ', TTF_GetError());
	texture := SDL_CreateTextureFromSurface(sdlRenderer, surface);
	rect^.w := surface^.w;
	rect^.h := surface^.h;
	x := rect^.x;
	y := rect^.y;
	if rect^.x < 0 then
		rect^.x := 640 - surface^.w + rect^.x;
	if rect^.y < 0 then
		rect^.y := 480 - surface^.h + rect^.y;

	if SDL_RenderCopy(sdlRenderer, texture, nil, rect) < 0 then
		writeln('failed copy: ', SDL_GetError());
	SDL_FreeSurface(surface);
	SDL_DestroyTexture(texture);
	rect^.x := x;
	rect^.y := y;
end;

function UT_TicksToStr(t : cardinal) : string;
var
	minutes, seconds, milliseconds : cardinal;

begin
	milliseconds := t mod 1000;
	seconds := (t-milliseconds) div 1000;
	minutes := ((t-milliseconds-seconds) div 1000) div 60;
	UT_TicksToStr := inttostr(minutes) + ':' + inttostr(seconds) + '.' + inttostr(milliseconds) + chr(0);
end;

end.
