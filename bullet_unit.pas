unit Bullet_unit;
interface
uses SDL2, GraphUtils;

type
	BU_Type = ( RED, BLUE, DISABLED );
	BU_Bullet = record
		x, y, sx, sy : single;
		t : BU_Type;
	end;

const
	MAXBULLETS = 999;

var
	BU_Bullets : array[1..MAXBULLETS] of BU_Bullet;
	BU_NumBullets : integer;	
	
procedure BU_Update(sdlEvent : PSDL_Event);
procedure BU_Draw(sdlRenderer : PSDL_Renderer);
procedure BU_Shoot(x, y, sx, sy : single; t : BU_Type);
procedure BU_Reset();
procedure BU_Disable(b : integer);

implementation
uses Player_unit, Enemy_unit;

var
	freeBullet : integer;
	
procedure BU_Shoot(x, y, sx, sy : single; t : BU_Type);
begin
	if BU_NumBullets >= MAXBULLETS then exit;
	while BU_Bullets[freeBullet].t <> DISABLED do
	begin
		freeBullet := freeBullet + 1;
		if freeBullet > MAXBULLETS then freeBullet := 1;
	end;
	BU_Bullets[freeBullet].x := x;
	BU_Bullets[freeBullet].y := y;
	BU_Bullets[freeBullet].t := t;
	BU_Bullets[freeBullet].sx := sx;
	BU_Bullets[freeBullet].sy := sy;
	freeBullet := freeBullet + 1;
	if freeBullet > MAXBULLETS then freeBullet := 1;
	BU_NumBullets := BU_NumBullets + 1;
end;

procedure BU_Disable(b : integer);
begin
	BU_Bullets[b].t := DISABLED;
	BU_NumBullets := BU_NumBullets - 1;
	freeBullet := b;
end;
	
procedure BU_Update(sdlEvent : PSDL_Event);
var
	i : integer;
	
begin
	for i := 1 to MAXBULLETS do
	begin
		if BU_Bullets[i].t <> DISABLED then
		begin
			BU_Bullets[i].x := BU_Bullets[i].x + BU_Bullets[i].sx;
			BU_Bullets[i].y := BU_Bullets[i].y + BU_Bullets[i].sy;
			if UT_Clampf(BU_Bullets[i].x, 0, 640) or UT_Clampf(BU_Bullets[i].y, 0, 480) then
			begin
				BU_Disable(i);
			end;
			if (BU_Bullets[i].t = BLUE) and
			   (abs(BU_Bullets[i].x - PL_PlayerObject^.x) < 2) and 
			   (abs(BU_Bullets[i].y - PL_PlayerObject^.y) < 2) then
			begin
				PL_PlayerObject^.life := PL_PlayerObject^.life - 1;
				BU_Disable(i);
			end;
			if BU_Bullets[i].t = BLUE then
				PL_PlayerObject^.score := PL_PlayerObject^.score + 1;
		end;
	end;
end;

procedure BU_Draw(sdlRenderer : PSDL_Renderer);
var
	rect : TSDL_Rect;
	i : integer;
	
begin
	rect.w := 3;
	rect.h := 3;
	for i := 1 to MAXBULLETS do
	begin
		if BU_Bullets[i].t <> DISABLED then
		begin
			rect.x := trunc(BU_Bullets[i].x) - 1;
			rect.y := trunc(BU_Bullets[i].y) - 1;
			case BU_Bullets[i].t of
				RED: SDL_SetRenderDrawColor(sdlRenderer, 255, 0, 0, 255);
				BLUE: SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 255, 255);
			end;
			SDL_RenderFillRect(sdlRenderer, @rect);
		end;
	end;
end;

procedure BU_Reset();
var
	i : integer;

begin
	for i := 1 to MAXBULLETS do
		BU_Bullets[i].t := DISABLED;
	BU_NumBullets := 0;
	freeBullet := 1;
end;

initialization
begin
	BU_Reset();
end;

end.
