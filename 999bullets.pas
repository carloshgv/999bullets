program bullets;

uses SDL2, SDL2_mixer, SDL2_image, SDL2_ttf, GraphUtils, Player_unit, Enemy_unit, Bullet_unit, sysutils;

var
	sdlWindow : PSDL_Window;
	sdlRenderer : PSDL_Renderer;
	event : PSDL_Event;
	quit : integer;
	music : PMIX_Music;
	startTime : cardinal;
	title : string;
	font : PTTF_Font;
	white : TSDL_Color;
	hiscore : cardinal;
	hiFile : file of cardinal;
	scoreRect, hiRect : TSDL_Rect;
	splashScreen : boolean = true;
	splashTexture : PSDL_texture;

begin
	//initilization of subsystems
	if SDL_Init(SDL_INIT_VIDEO or SDL_INIT_AUDIO) < 0 then HALT;
	randomize;

	sdlWindow := SDL_CreateWindow('0bullets', 50, 50, 640, 480, SDL_WINDOW_SHOWN);
	if sdlWindow = nil then halt;

	sdlRenderer := SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED or SDL_RENDERER_PRESENTVSYNC);
	if sdlRenderer = nil then halt;

	if MIX_OpenAudio( 44100, AUDIO_S16LSB, 2, 2048 ) < 0 then halt;
	music := MIX_LoadMus('afternoon.mp3');
	if music = nil then
		writeln('failed loading afternoon.mp3: ', MIX_GetError());
	TTF_Init();
	font := TTF_OpenFont('retro_party.ttf', 16);
	if font = nil then
		writeln('failed loading retro_party.ttf: ', TTF_GetError());
	splashTexture := IMG_LoadTexture(sdlRenderer, 'splash.png');
	if splashTexture = nil then
		writeln('failed loading splash screen', IMG_GetError());

	white.r := 255;
	white.g := 255;
	white.b := 255;
	assign(hiFile, 'hiscore');
	{$I-}
	reset(hiFile);
	read(hiFile, hiscore);
	if IOResult <> 0 then
		hiscore := 0;
	close(hiFile);
	{$I+}
	rewrite(hiFile);

	// game initialization
	MIX_PlayMusic(music, -1);
	scoreRect.x := 2;
	scoreRect.y := 2;
	hiRect.x := -2;
	hiRect.y := 2;

	new(PL_PlayerObject);
	PL_PlayerObject^.x := 320;
	PL_PlayerObject^.y := 400;
	PL_PlayerObject^.life := 1;
	PL_PlayerObject^.score := 0;
	PL_PlayerObject^.time := 0;
	PL_PlayerObject^.hits := 0;
	PL_LoadResources();

	EN_Reset();
	EN_LoadResources();

	// main loop
	quit := 0;
	new(event);
	startTime := SDL_GetTicks();
	repeat
		title := inttostr(BU_NumBullets) + 'bullets   ';
		SDL_SetWindowTitle(sdlWindow, pchar(@title)+1);
		PL_PlayerObject^.time := SDL_GetTicks() - startTime;

		SDL_SetRenderDrawColor( sdlRenderer, 230, 230, 240, 255 );
		SDL_RenderClear( sdlRenderer );

		SDL_PollEvent(event);
		// Update
		if not PL_Update(event) then
		begin
			BU_Reset();
			EN_Reset();
			startTime := SDL_GetTicks();
			if PL_PlayerObject^.score > hiscore then
				hiscore := PL_PlayerObject^.score;
			splashScreen := true;
			continue;
		end;
		if not splashScreen then
		begin
			BU_Update(event);
			EN_Update(event);
		end;

		case event^.type_ of
			SDL_QUITEV:	quit := 1;
			SDL_KEYDOWN:
			begin
				if event^.key.keysym.sym = SDLK_ESCAPE then quit := 1;
				if event^.key.keysym.sym = byte(SDLK_Z) then
				begin
					if splashScreen then
					begin
						splashScreen := false;
						PL_PlayerObject^.score := 0;
					end;
				end;
			end;
		end;

		// Draw
		if splashScreen then
		begin
			SDL_RenderCopy(sdlRenderer, splashTexture, nil, nil);
		end
		else
		begin
			EN_Draw(sdlRenderer);
			BU_Draw(sdlRenderer);
		end;
		UT_Write(sdlRenderer, font, inttostr(PL_PlayerObject^.score) + chr(0), @scoreRect, @white);
		UT_Write(sdlRenderer, font, inttostr(hiscore) + chr(0), @hiRect, @white);
		PL_Draw(sdlRenderer);
		SDL_RenderPresent(sdlRenderer);
	until quit = 1;
	// finalization
	write(hiFile, hiscore);
	dispose(PL_PlayerObject);
	close(hiFile);
	MIX_FreeMusic(music);
	SDL_Quit;
end.
