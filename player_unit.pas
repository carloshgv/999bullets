unit Player_unit;

interface
uses SDL2, GraphUtils;

type
	PL_Player = record
		x, y : integer;
		life : integer;
		score, hits, time : cardinal;
	end;
	PPL_Player = ^PL_Player;
	
var
	PL_PlayerObject : PPL_Player;

procedure PL_Draw(sdlRenderer : PSDL_Renderer);
function PL_Update(sdlEvent : PSDL_Event) : boolean;
procedure PL_LoadResources();
	
implementation
uses Enemy_unit, Bullet_unit, SDL2_mixer;

var
	shipPoly : array[1..3] of PUT_Poly;
	angularSpeed : array[1..3] of real;
	laser, dead : PMIX_Chunk;

procedure PL_LoadResources();
begin
	laser := MIX_LoadWav('laser.wav');
	if laser = nil then
		writeln('failed loading laser.wav: ', MIX_GetError());
		
	dead := MIX_LoadWav('dead.wav');
	if dead = nil then
		writeln('failed loading dead.wav: ', MIX_GetError());
end;
	
procedure PL_draw(sdlRenderer : PSDL_Renderer);
var
	i : integer;
	rect : TSDL_Rect;
	
begin
	rect.x := PL_PlayerObject^.x-1;
	rect.y := PL_PlayerObject^.y-1;
	rect.w := 3;
	rect.h := 3;
	SDL_SetRenderDrawColor(sdlRenderer, 255, 0, 0, 255);
	SDL_RenderFillRect(sdlRenderer, @rect);
	for i := 1 to 3 do
	begin
		shipPoly[i]^.x := PL_PlayerObject^.x;
		shipPoly[i]^.y := PL_PlayerObject^.y;

		SDL_SetRenderDrawColor(sdlRenderer, 255, 0, 255, 255);
		UT_DrawPoly(sdlRenderer, shipPoly[i]);
	end;
end;

function PL_Update(sdlEvent : PSDL_Event) : boolean;
var
	i : integer;
	keyboard : ^byte;

begin
	if PL_PlayerObject^.life <= 0 then
	begin
		MIX_PlayChannel(-1, dead, 0);
		SDL_Delay(2000);
		PL_PlayerObject^.life := 1;
		exit(false);
	end;

	keyboard := SDL_GetKeyBoardState(nil);
	
	if keyboard[SDL_SCANCODE_DOWN] = 1 then PL_PlayerObject^.y := PL_PlayerObject^.y + 3
	else if keyboard[SDL_SCANCODE_UP] = 1 then PL_PlayerObject^.y := PL_PlayerObject^.y - 3;
	if keyboard[SDL_SCANCODE_RIGHT] = 1 then PL_PlayerObject^.x := PL_PlayerObject^.x + 3
	else if keyboard[SDL_SCANCODE_LEFT] = 1 then PL_PlayerObject^.x := PL_PlayerObject^.x - 3;
	UT_Clamp(PL_PlayerObject^.x, 0, 640);
	UT_Clamp(PL_PlayerObject^.y, 0, 480);
	
	if (keyboard[SDL_SCANCODE_Z] = 1) and (BU_NumBullets < MAXBULLETS) then begin
		MIX_PlayChannel(-1, laser, 0);
		BU_Shoot(PL_PlayerObject^.x, PL_PlayerObject^.y, 0, -6, RED);
	end;
		
	for i := 1 to 3 do
	begin
		shipPoly[i]^.angle := shipPoly[i]^.angle + angularSpeed[i];
	end;
	PL_Update := true;
end;

initialization
begin
	new(shipPoly[1]);
	shipPoly[1]^.sides := 7;
	shipPoly[1]^.radius := 15;
	shipPoly[1]^.angle := 0.2;
	new(shipPoly[2]);
	shipPoly[2]^.sides := 5;
	shipPoly[2]^.radius := 16;
	shipPoly[2]^.angle := 0.4;
	new(shipPoly[3]);
	shipPoly[3]^.sides := 4;
	shipPoly[3]^.radius := 13;
	shipPoly[3]^.angle := 0;
	
	angularSpeed[1] := -0.01;
	angularSpeed[2] := 0.02;
	angularSpeed[3] := -0.1;
end;

finalization
begin
	dispose(shipPoly[1]);
	dispose(shipPoly[2]);
	dispose(shipPoly[3]);
	
	MIX_FreeChunk(laser);
	MIX_FreeChunk(dead);
end;
end.
