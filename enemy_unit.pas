unit Enemy_unit;
interface
uses SDL2, GraphUtils;

const
	MAXENEMIES = 20;
	
type
	EN_State = ( ACTIVE, INACTIVE );
	EN_Enemy = record
		x, y : integer;
		sx, sy : integer;
		timeBetweenShots, cooldown : integer;
		state : EN_State;
	end;

var
	EN_Enemies : array[1..MAXENEMIES] of EN_Enemy;
	EN_NumEnemies : integer;
	
procedure EN_Update(sdlEvent : PSDL_Event);
procedure EN_Draw(sdlRenderer : PSDL_Renderer );
procedure EN_Reset();
procedure EN_LoadResources();

implementation
uses Player_unit, Bullet_unit, SDL2_mixer;

var
	hit : PMIX_Chunk;
	
procedure EN_Init(var en : EN_Enemy);
begin
	en.x := random(630) + 5;
	en.y := random(470) + 5;
	en.sx := random(4) - 2;
	en.sy := random(2) - 1;
	en.timeBetweenShots := random(15) + 15;
	en.cooldown := en.timeBetweenShots;
	en.state := ACTIVE;
end;

procedure EN_Reset();
var
	i : integer;
begin
	for i := 1 to MAXENEMIES do
		EN_Enemies[i].state := INACTIVE;
	EN_Init(EN_Enemies[1]);
	EN_NumEnemies := 1;
end;

procedure EN_LoadResources();
begin
	hit := MIX_LoadWav('hit.wav');
	if hit = nil then
		writeln('failed loading hit.wav: ', MIX_GetError());
end;

procedure EN_Update(sdlEvent : PSDL_Event);
var
	i, j : integer;
	en : ^EN_Enemy;
	bu : ^BU_Bullet;
	bsx, bsy, dist : float;
	
begin
	for i := 1 to EN_NumEnemies do
	begin
		PL_PlayerObject^.score := PL_PlayerObject^.score + 5;
		en := @EN_Enemies[i];
		en^.x := en^.x + en^.sx;
		en^.y := en^.y + en^.sy;
		if UT_Clamp(en^.x, 10, 630) then en^.sx := -en^.sx;
		if UT_Clamp(en^.y, 10, 470) then en^.sy := -en^.sy;
		
		for j := 1 to MAXBULLETS do
		begin
			bu := @BU_Bullets[j];
			if (bu^.t = RED) and (bu^.x > en^.x - 7) and (bu^.x < en^.x + 7)
						     and (bu^.y > en^.y - 7) and (bu^.y < en^.y + 7) then
			begin
				MIX_PlayChannel(-1, hit, 0);
				PL_PlayerObject^.score := PL_PlayerObject^.score + 10000;
				EN_Init(en^);
				if EN_NumEnemies < MAXENEMIES then
				begin
					EN_NumEnemies := EN_NumEnemies + 1;
					EN_Init(EN_Enemies[EN_NumEnemies]);
				end;
				BU_Disable(j);
			end;
		end;
		
		en^.cooldown := en^.cooldown - 1;
		if en^.cooldown <= 0 then
		begin
			bsx := PL_PlayerObject^.x - en^.x;
			bsy := PL_PlayerObject^.y - en^.y;
			dist := sqrt(bsx*bsx + bsy*bsy);
			bsx := (bsx / dist);
			bsy := (bsy / dist);
			BU_Shoot(en^.x, en^.y, bsx, bsy, BLUE);
			en^.cooldown := en^.timeBetweenShots;
		end;	
	end;
end;

procedure EN_Draw(sdlRenderer : PSDL_Renderer);
var
	rect : TSDL_Rect;
	i : integer;
	en : ^EN_Enemy;
begin
	rect.w := 15;
	rect.h := 15;
	for i := 1 to EN_NumEnemies do
	begin
		en := @EN_Enemies[i];
		rect.x := en^.x - 7;
		rect.y := en^.y - 7;
		SDL_SetRenderDrawColor(sdlRenderer, 240, 240, 240, 100);
		SDL_RenderFillRect(sdlRenderer, @rect);
		SDL_SetRenderDrawColor(sdlRenderer, 100, 200, 240, 255);
		SDL_RenderDrawRect(sdlRenderer, @rect);
	end;
end;

initialization
begin
	EN_NumEnemies := 0;
end;

finalization
begin
	MIX_FreeChunk(hit);
end;
	
end.
